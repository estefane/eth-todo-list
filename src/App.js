import react, { useState, useEffect } from 'react'
import Web3 from 'web3'
import './App.css';
import { TODO_LIST_ABI, TODO_LIST_ADDRESS } from './config';
import Navbar from './components/Navbar'
import ToDoList from './components/ToDoList';

function App() {
  const [account, setAccount] = useState();
  const [taskCount, setTaskCount] = useState(0);
  const [tasks, setTasks] = useState([]);
  const [loading, setLoading] = useState(true);
  const [toDoList, setToDoList] = useState({});

  async function loadBlockchainData(){
    const web3 = new Web3(Web3.givenProvider || 'http://localhost:8545')
    
    const accounts = await web3.eth.getAccounts();
    setAccount(accounts[0]);

    const todoList = new web3.eth.Contract(TODO_LIST_ABI, TODO_LIST_ADDRESS);
    setToDoList(todoList);
    
    const taskCount = await todoList.methods.taskCount().call();
    setTaskCount(taskCount);

    for(let i = 1; i <= taskCount; i++) {
      const task = await todoList.methods.tasks(i).call();
      setTasks([...tasks, task]);
    }

    setLoading(false);
  }

  function createTasks(content){
    setLoading(true);
    toDoList.methods.createTask(content).send({from: account})
    .once('receipt', (receipt) => {
      setLoading(false);
    })
  }

  function toggleCompleted(taskId){
    setLoading(true);
    toDoList.methods.toggleCompleted(taskId).send({from: account})
    .once('receipt', (receipt) => {
      setLoading(false);
    })
  }

  useEffect(() => {
    loadBlockchainData();
  }, []);

  return (
    <>
      <Navbar
        title="To Do List"
        account={account}
      />
      <main>
        { loading 
          ? <p>Loading...</p> 
          : <ToDoList 
              tasks={tasks} 
              createTasks={createTasks}
              toggleCompleted={toggleCompleted}
            />
        }
      </main>
    </>
  );
}

export default App;
