import react, { useRef } from 'react';

export default function ToDoList({tasks, createTasks, toggleCompleted}) {
    const task = useRef();
    const check = useRef();

    const onSubmit = (e) => {
      e.preventDefault();
      createTasks(task.current.value);
    }

    return (
        <div className="container">
          <form 
            id="add-task" 
            onSubmit={(e)=>onSubmit(e)}
          >
            <input 
              ref={task} 
              placeholder="Add task..."
              required
            />
            <button type="submit">Submit</button>
          </form>

          <ul>
            {tasks.map((task, key)=> (
                <li className="task" key={key}>
                  <input 
                    type="checkbox"
                    name={task.id}
                    ref={check}
                    defaultChecked={task.completed}
                    onClick={() => toggleCompleted(check.current.name)}
                  />
                  <label>{task.content}</label>
                </li>
              ))
            }
          </ul>

          <ul id="completed-tasklist">
          </ul>
        </div>
    )
}