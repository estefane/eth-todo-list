import './styles.css'

export default function Navbar({ title, account }) {
  return (    
    <nav>
        <h2>{title}</h2>
        <p>{account}</p>
    </nav>
  );
}